
import Navbar from './Navbar';

const App = () => {
  return (
    <main id='home'>
      <Navbar />
      <section className='home'>
        <h1>home</h1>
      </section>
      <section className='about' id='about'>
        <h1>about</h1>
      </section>
      <section className='clients' id='clients'>
        <h1>clients</h1>
      </section>
      <section className='contact' id='contact'>
        <h1>contact</h1>
      </section>
    </main>
  )
};

export default App;
